<?php
require 'vendor/autoload.php';

echo "
<pre>
   ___        ___     _           _                                  _   
  / _ \/\  /\/ _ \   /_\  ___ ___(_) __ _ _ __  _ __ ___   ___ _ __ | |_ 
 / /_)/ /_/ / /_)/  //_\\/ __/ __| |/ _` | '_ \| '_ ` _ \ / _ \ '_ \| __|
/ ___/ __  / ___/  /  _  \__ \__ \ | (_| | | | | | | | | |  __/ | | | |_ 
\/   \/ /_/\/      \_/ \_/___/___/_|\__, |_| |_|_| |_| |_|\___|_| |_|\__|
                                    |___/                                                                       
</pre>
";

use Src\MyExampleObject;

$object = new MyExampleObject('PHP Assignment Example');
echo $object->getTitle();

